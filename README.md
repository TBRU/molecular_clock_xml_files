# molecular_clock_xml_files

xml input files for Beast, associated with manuscript: The Molecular Clock of Mycobacterium tuberculosis. 
DOI:  https://doi.org/10.1371/journal.ppat.1008067

Analysis with exponential growth population prior, and 1/x prior on the clock rate